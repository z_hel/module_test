﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework.Interfaces;

namespace BigFigure
{
    [TestFixture]
    public class FigureTest
    {
        [Test]
        public void EnterStringTest()
        {
            MyInt f = new MyInt("123456");

            Assert.IsNotNull(f);
        }

        [Test]
        public void EnterIntTest()
        {
            MyInt f_int = new MyInt(123456);

            Assert.IsNotNull(f_int);
        }

        [Test]
        public void EnterByteTest()
        {
            byte[] numbers = { 0, 1, 0, 1 };
            MyInt f_byte = new MyInt(numbers);

            Assert.IsNotNull(f_byte);
        }


        [Test]
        public void AddTest()
        {
            MyInt f = new MyInt("123456");
            MyInt f1 = new MyInt("11");
            MyInt res = new MyInt("123467");
            Assert.AreEqual("123467", f.Add(f1));
        }

        [Test]
        public void SubstractTest()
        {
            MyInt f = new MyInt("123456");
            MyInt f1 = new MyInt("1");
            Assert.AreEqual("123455", f.Substract(f1));
        }

        [Test]
        public void MultiplyTest()
        {
            MyInt f = new MyInt("123456");
            MyInt f1 = new MyInt("1");
            MyInt mult = f.Multiply(f1);
            //Assert.AreEqual(f.toString(), mult.toString());
        }

        [Test]
        public void MaxTest()
        {
            MyInt f = new MyInt("123456");
            MyInt f1 = new MyInt("1");
            MyInt max_f = f.Max(f1);
            //Assert.AreEqual(max_f, f);
        }

        [Test]
        public void MinTest()
        {
            MyInt f = new MyInt("123456");
            MyInt f1 = new MyInt("1");
            MyInt min_f = f.Min(f1);
            //Assert.AreEqual(min_f, f1);
        }

        [Test]
        public void AbsTest()
        {
            //MyInt f = new MyInt("-123456");
            MyInt f = new MyInt("123456");
            MyInt f1 = new MyInt("123456");
            MyInt f_abs = f.Abs();
            //Assert.AreEqual(f_abs, f1);
        }

        [Test]
        public void CompareToTest()
        {
            MyInt f = new MyInt("123456");
            MyInt f1 = new MyInt("1");
            f.CompareTo(f1);
            //Assert.AreEquals(f_abs, f1);
        }

        [Test]
        public void GcdTest()
        {
            MyInt f = new MyInt("123456");
            f.Gcd();
        }

        [Test]
        public void toStringTest()
        {
            MyInt f = new MyInt("123456");
            f.toString();
        }

        [Test]
        public void longValueTest()
        {
            MyInt f = new MyInt("123456");
            f.longValue();
        }
    }
}
