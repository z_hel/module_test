﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigFigure
{
    class MyInt
    {
        private List<int> figure = new List<int>();
        public MyInt(string figure_str)
        {
            for (int i = 0; i < figure_str.Length; i++) {
                figure.Add(int.Parse(figure_str[i].ToString()));
            }
        }

        public MyInt(int figure_int)
        {
            string str = figure_int.ToString();
            for (int i = 0; i < str.Length; i++) {
                figure.Add(int.Parse(str[i].ToString()));
            }
        }

        public MyInt(byte[] figure_int_mass)
        {
            for (int i = 0; i < figure_int_mass.Length; i++) {
                figure.Add(figure_int_mass[i]);
            }
        }



        public MyInt Add(MyInt f)
        {
            f.figure.Reverse();
            figure.Reverse();
            int length = 0;
            if (figure.Count() > f.figure.Count())
            {
                length = figure.Count() + 1;
                for (int i = 1; i <= length - f.figure.Count()+1; i++)
                {
                    f.figure.Add(0);
                    
                }
                figure.Add(0);
            }
            else
            {
                length = f.figure.Count() + 1;
                for (int i = 1; i <= length - figure.Count()+1; i++)
                {
                    figure.Add(0);
                }
                f.figure.Add(0);
            }
            /*
            for (int h = 0; h < figure.Count(); h++)
                Console.WriteLine(length + " " + figure[h]);
            Console.WriteLine("\n");
            for (int h = 0; h < f.figure.Count(); h++)
                Console.WriteLine(length + " " + f.figure[h]);*/

            
            
            for (int ix = 0; ix < length-1; ix++) {
                f.figure[ix] += figure[ix]; // суммируем последние разряды чисел
                f.figure[ix + 1] += (f.figure[ix] / 10); // если есть разряд для переноса, переносим его в следующий разряд
                f.figure[ix] %= 10; // если есть разряд для переноса он отсекается
            }

            if (f.figure[length - 1] == 0)
                length--;
            f.figure.Reverse();
            /*
            string str = "";
            for (int i = 0; i < f.figure.Count(); i++)
            {
                str += f.figure[i];
            }*/
            //MyInt myInt = new MyInt(str);

            return f;
        }

        public MyInt Substract(MyInt f)
        {
            f.figure.Reverse();
            figure.Reverse();
            int k = 3; //если к == 3, числа одинаковой длины
            int length = figure.Count();

            if (figure.Count() > f.figure.Count())
            {
                length = figure.Count();
                k = 1; //если к == 1, первое число длиннее второго
                for (int i = 1; i <= length - f.figure.Count() + 1; i++)
                {
                    f.figure.Add(0);

                }


                /*
                for (int h = 0; h < figure.Count(); h++)
                    Console.WriteLine(length + " " + figure[h]);
                Console.WriteLine("\n");
                for (int h = 0; h < f.figure.Count(); h++)
                    Console.WriteLine(length + " " + f.figure[h]);
                */



            }
            else if (f.figure.Count() > figure.Count())
            {
                    length = f.figure.Count();
                    k = 2; //если к == 2, второе число длиннее первого
                    for (int i = 1; i <= length - figure.Count() + 1; i++)
                    {
                        figure.Add(0);

                    }


                    /*
                for (int h = 0; h < figure.Count(); h++)
                    Console.WriteLine(length + " " + figure[h]);
                Console.WriteLine("\n");
                for (int h = 0; h < f.figure.Count(); h++)
                    Console.WriteLine(length + " " + f.figure[h]);
                */



            }
            else //если числа одинаковой длины, то необходимо сравнить их веса
            {
                    for (int ix = 0; ix < length; ix++) //поразрядное сравнение весов чисел
                    {
                        if (figure[ix] > f.figure[ix]) //если разряд первого числа больше
                        {
                            k = 1; //первое число длиннее второго
                            for (int i = 1; i <= length - f.figure.Count() + 1; i++)
                            {
                                f.figure.Add(0);

                            }


                            /*
                        for (int h = 0; h < figure.Count(); h++)
                            Console.WriteLine(length + " " + figure[h]);
                        Console.WriteLine("\n");
                        for (int h = 0; h < f.figure.Count(); h++)
                            Console.WriteLine(length + " " + f.figure[h]);
                        */



                        break;
                        }

                        if (f.figure[ix] > figure[ix]) // если разряд второго числа больше
                        {
                            k = 2; //второе число длиннее первого
                            for (int i = 1; i <= length - figure.Count() + 1; i++)
                            {
                                figure.Add(0);

                            }


                            /*
                        for (int h = 0; h < figure.Count(); h++)
                            Console.WriteLine(length + " " + figure[h]);
                        Console.WriteLine("\n");
                        for (int h = 0; h < f.figure.Count(); h++)
                            Console.WriteLine(length + " " + f.figure[h]);
                        */



                        break;
                        }
                    }
            }

            //figure.Reverse();
            string str = "";
            for (int i = 0; i < figure.Count(); i++)
            {
                str += figure[i];
            }

            MyInt myFigure = new MyInt(str);
            if (k==1)
            {
                return substr(myFigure, f, length);
            }
            else if (k == 2)
            {
                return substr(f, myFigure, length);
            }

            return myFigure;
        }
        //
        public MyInt substr(MyInt f1, MyInt f2, int length)
        {
            List<int> res = new List<int>();
            for (int y = 0; y < length; y++)
            {
                res.Add(0);
            }
            
            string str = "";
            
            for (int ix = 0; ix < (length - 1); ix++) // проход по всем разрядам числа, начиная с последнего, не доходя до первого
            {
                if (ix < (length - 1)) // если текущий разряд чисел не первый
                {
                    f1.figure[ix + 1]--; // в следующуем разряде большего числа занимаем 1.
                    res[ix] += 10 + f1.figure[ix]; // в ответ записываем сумму значения текущего разряда большего числа и 10-ти

                }
                else  // если текущий разряд чисел - первый
                    res[ix] += f1.figure[ix]; // в ответ суммируем значение текущего разряда большего числа
                res[ix] -= f2.figure[ix]; // вычитаем значение текущего разряда меньшего числа

                if (res[ix] / 10 > 0) // если значение в текущем разряде двухразрядное
                {
                    res[ix + 1]++; // переносим единицу в старший разряд
                    res[ix] %= 10; // в текущем разряде отсекаем ее
                }
                
            }
            for (int i = 0; i < res.Count(); i++)
            {
                str = res[i] + str;
            }
            
            MyInt myInt = new MyInt(str);
            return myInt;
        }
        //
        public MyInt Multiply(MyInt f)
        {

            return f;
        }

        public MyInt Max(MyInt f)
        {
            if (figure.Count() > f.figure.Count())
            {
                string str = "";
                for (int i = 0; i < figure.Count(); i++)
                {
                    str += figure[i];
                }
                MyInt myInt = new MyInt(str);
                return myInt;
            }
            else if (figure.Count() < f.figure.Count())
            {
                return f;
            }
            else if (figure.Count() == f.figure.Count())
            {
                for (int i = 0; i < figure.Count(); i++)
                {
                    if (figure[i] > f.figure[i])
                    {
                        string str = "";
                        for (int j = 0; j < figure.Count(); j++)
                        {
                            str += figure[j];
                        }
                        MyInt myInt = new MyInt(figure.ToString());
                        return myInt;
                    }
                    else if (figure[i] < f.figure[i]) return f;
                    else if (figure[i] == f.figure[i])
                    {
                        MyInt f1 = new MyInt(figure[i].ToString());
                        MyInt f2 = new MyInt(f.figure[i].ToString());
                        f1.Max(f2);
                    }
                }
            }
            return f;
        }

        public MyInt Min(MyInt f)
        {

            return f;
        }

        public MyInt Abs()
        {
            MyInt f = new MyInt("111");
            return f;
        }

        public MyInt CompareTo(MyInt f)
        {

            return f;
        }

        public MyInt Gcd()
        {
            MyInt f = new MyInt("111");
            return f;
        }

        public string toString()
        {
            string str = "";
            //MyInt f = new MyInt("111");
            for (int i = 0; i < figure.Count(); i++)
            {
                str += figure[i]; 
            }
            return str;
        }

        public MyInt longValue()
        {
            MyInt f = new MyInt("111");
            return f;
        }
    }

}
